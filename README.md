# Kafka on alpine linux

The aim here is to create a minimal apache kafka image which will stand pu to
CIS docker security benchmarks.

## Building

```sh
  make
```

## Usage

### Standalone node

* start zookeeper
* modify server0.properties with zookeeper details
* mount server0.properties

```bash
  docker run \
  -v $(pwd)/config/server0.properties:/opt/kafka/config/server.properties \
  -t nathamanath/kafka:latest
```

### Kafka cluster

docker-compose.yml shows this image used to set up a kafka cluster. Required
config is in `./configs`. `docker-compose up` should get this going.

```bash
  # start the cluster
  docker-compose up

  # create a topic
  docker-compose exec kafka1 bin/kafka-topics.sh --create --zookeeper zoo1:2181 --replication-factor 3 --partitions 8 --topic tester

  # produce messages for this topic
  docker-compose exec kafka1 bin/kafka-console-producer.sh --broker-list kafka1:9092 kafka2:9092 kafka3:9092 --topic tester

  # consume this topic
  docker-compose exec kafka1 bin/kafka-console-consumer.sh --bootstrap-server kafka1:9092 --from-beginning --topic tester
```
